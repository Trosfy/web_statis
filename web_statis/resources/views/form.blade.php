<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=h1, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label for="user_first_name">First Name:</label><br><br>
        <input type="text" name="user_first_name" id=""><br><br>
        <label for="user_last_name">Last Name:</label><br><br>
        <input type="text" name="user_last_name" id=""><br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio" name="gender" id="">Male <br>
        <input type="radio" name="gender" id="">Female <br>
        <input type="radio" name="gender" id="">Other <br><br>
        <label for="">Nationality:</label><br><br>
        <select name="nationality" id="">
            <option value="">Indonesian</option><br>
            <option value="">Singaporean</option><br>
            <option value="">Australian</option><br>
            <option value="">American</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name="language" id="">Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="">English <br>
        <input type="checkbox" name="language" id="">Other <br><br>
        <label for="">Bio:</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>