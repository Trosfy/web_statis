<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    function register()
    {
        return view('form');
    }
    function welcome(Request $request)
    {
        // dd($request['user_first_name']);
        $first_name = $request['user_first_name'];
        $last_name = $request['user_last_name'];
        return view('welcome', compact('first_name','last_name'));
    }
}
